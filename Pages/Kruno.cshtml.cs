﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Zadatak1.Pages
{
    public class KrunoModel : PageModel
    {
        private readonly ILogger<KrunoModel> _logger;

        public KrunoModel(ILogger<KrunoModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            Ime = "Kruno";
        }

        public string Ime { get; set; }
    }
}
